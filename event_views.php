<?php

/**
 * @file
 * Exported View.
 */

$view = new view();
$view->name = 'event_category_blocks';
$view->description = 'Displays news item categories.';
$view->tag = 'news';
$view->base_table = 'taxonomy_term_data';
$view->human_name = 'Event category blocks';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Events by Audience';
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 0;
/* Relationship: Taxonomy term: Content using Audience */
$handler->display->display_options['relationships']['reverse_field_audience_node']['id'] = 'reverse_field_audience_node';
$handler->display->display_options['relationships']['reverse_field_audience_node']['table'] = 'taxonomy_term_data';
$handler->display->display_options['relationships']['reverse_field_audience_node']['field'] = 'reverse_field_audience_node';
$handler->display->display_options['relationships']['reverse_field_audience_node']['required'] = 1;
/* Field: Taxonomy term: Term ID */
$handler->display->display_options['fields']['tid']['id'] = 'tid';
$handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['tid']['field'] = 'tid';
$handler->display->display_options['fields']['tid']['label'] = '';
$handler->display->display_options['fields']['tid']['exclude'] = TRUE;
$handler->display->display_options['fields']['tid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['tid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['tid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['tid']['alter']['external'] = 0;
$handler->display->display_options['fields']['tid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['tid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['tid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['tid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['tid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['tid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['tid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['tid']['alter']['html'] = 0;
$handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['tid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['tid']['hide_empty'] = 0;
$handler->display->display_options['fields']['tid']['empty_zero'] = 0;
$handler->display->display_options['fields']['tid']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['tid']['format_plural'] = 0;
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['exclude'] = TRUE;
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['name']['alter']['path'] = 'events/category/[tid]';
$handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name']['alter']['external'] = 0;
$handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = 0;
/* Field: COUNT(Content: Nid) */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['relationship'] = 'reverse_field_audience_node';
$handler->display->display_options['fields']['nid']['group_type'] = 'count';
$handler->display->display_options['fields']['nid']['label'] = '';
$handler->display->display_options['fields']['nid']['alter']['alter_text'] = 1;
$handler->display->display_options['fields']['nid']['alter']['text'] = '[name] [nid]';
$handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nid']['alter']['external'] = 0;
$handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nid']['alter']['html'] = 0;
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nid']['hide_empty'] = 0;
$handler->display->display_options['fields']['nid']['empty_zero'] = 0;
$handler->display->display_options['fields']['nid']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['nid']['format_plural'] = 0;
$handler->display->display_options['fields']['nid']['prefix'] = '(';
$handler->display->display_options['fields']['nid']['suffix'] = ')';
/* Filter criterion: Taxonomy vocabulary: Machine name */
$handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
$handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['value'] = array(
  'uwaterloo_audience' => 'uwaterloo_audience',
);
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['relationship'] = 'reverse_field_audience_node';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_event' => 'uw_event',
);

/* Display: Block - News by Audience */
$handler = $view->new_display('block', 'Block - News by Audience', 'block');


/* ---------------------------- */


$view = new view();
$view->name = 'events';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Events';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Upcoming Events';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '25';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Content revision: User */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node_revision';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Date and time */
$handler->display->display_options['fields']['field_event_date']['id'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['label'] = '';
$handler->display->display_options['fields']['field_event_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_event_date']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_event_date']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_event_date']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_event_date']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_event_date']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_event_date']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_event_date']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['field_event_date']['settings'] = array(
  'format_type' => 'long_date_only',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_event_date']['group_rows'] = 0;
$handler->display->display_options['fields']['field_event_date']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_event_date']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_event_date']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_event_date']['field_api_classes'] = 0;
/* Field: Event start time */
$handler->display->display_options['fields']['field_event_date_1']['id'] = 'field_event_date_1';
$handler->display->display_options['fields']['field_event_date_1']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date_1']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date_1']['ui_name'] = 'Event start time';
$handler->display->display_options['fields']['field_event_date_1']['label'] = '';
$handler->display->display_options['fields']['field_event_date_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_event_date_1']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_event_date_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_event_date_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_event_date_1']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_event_date_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['settings'] = array(
  'format_type' => 'time_only',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_event_date_1']['group_rows'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_event_date_1']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_event_date_1']['field_api_classes'] = 0;
/* Field: Event end time */
$handler->display->display_options['fields']['field_event_date_2']['id'] = 'field_event_date_2';
$handler->display->display_options['fields']['field_event_date_2']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date_2']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date_2']['ui_name'] = 'Event end time';
$handler->display->display_options['fields']['field_event_date_2']['label'] = '';
$handler->display->display_options['fields']['field_event_date_2']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_event_date_2']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_event_date_2']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_event_date_2']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_event_date_2']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_event_date_2']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['settings'] = array(
  'format_type' => 'time_only',
  'fromto' => 'value2',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_event_date_2']['group_rows'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_event_date_2']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_event_date_2']['field_api_classes'] = 0;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_event_date] from [field_event_date_1] to [field_event_date_2]';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['body']['alter']['external'] = 0;
$handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['body']['alter']['trim'] = 0;
$handler->display->display_options['fields']['body']['alter']['html'] = 0;
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['element_default_classes'] = 1;
$handler->display->display_options['fields']['body']['hide_empty'] = 0;
$handler->display->display_options['fields']['body']['empty_zero'] = 0;
$handler->display->display_options['fields']['body']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
$handler->display->display_options['fields']['body']['settings'] = array(
  'trim_length' => '300',
);
$handler->display->display_options['fields']['body']['field_api_classes'] = 0;
/* Field: Content: Link */
$handler->display->display_options['fields']['view_node']['id'] = 'view_node';
$handler->display->display_options['fields']['view_node']['table'] = 'node';
$handler->display->display_options['fields']['view_node']['field'] = 'view_node';
$handler->display->display_options['fields']['view_node']['label'] = '';
$handler->display->display_options['fields']['view_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['view_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['view_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['view_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['view_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['view_node']['text'] = 'Read more';
/* Sort criterion: Content: Sticky */
$handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
$handler->display->display_options['sorts']['sticky']['table'] = 'node';
$handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
$handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
/* Sort criterion: Content: Date and time -  start date (field_event_date) */
$handler->display->display_options['sorts']['field_event_date_value']['id'] = 'field_event_date_value';
$handler->display->display_options['sorts']['field_event_date_value']['table'] = 'field_data_field_event_date';
$handler->display->display_options['sorts']['field_event_date_value']['field'] = 'field_event_date_value';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_event' => 'uw_event',
);
/* Filter criterion: Content: Date and time -  start date (field_event_date) */
$handler->display->display_options['filters']['field_event_date_value']['id'] = 'field_event_date_value';
$handler->display->display_options['filters']['field_event_date_value']['table'] = 'field_data_field_event_date';
$handler->display->display_options['filters']['field_event_date_value']['field'] = 'field_event_date_value';
$handler->display->display_options['filters']['field_event_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_event_date_value']['default_date'] = 'now';
$handler->display->display_options['filters']['field_event_date_value']['add_delta'] = 'yes';

/* Display: Events - List */
$handler = $view->new_display('page', 'Events - List', 'page');
$handler->display->display_options['defaults']['group_by'] = FALSE;
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '3';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Date and time */
$handler->display->display_options['fields']['field_event_date']['id'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['label'] = '';
$handler->display->display_options['fields']['field_event_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_event_date']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_event_date']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_event_date']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_event_date']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_event_date']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_event_date']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_event_date']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['field_event_date']['settings'] = array(
  'format_type' => 'long_date_only',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_event_date']['group_rows'] = 0;
$handler->display->display_options['fields']['field_event_date']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_event_date']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_event_date']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_event_date']['field_api_classes'] = 0;
/* Field: Event start time */
$handler->display->display_options['fields']['field_event_date_1']['id'] = 'field_event_date_1';
$handler->display->display_options['fields']['field_event_date_1']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date_1']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date_1']['ui_name'] = 'Event start time';
$handler->display->display_options['fields']['field_event_date_1']['label'] = '';
$handler->display->display_options['fields']['field_event_date_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_event_date_1']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_event_date_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_event_date_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_event_date_1']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_event_date_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['settings'] = array(
  'format_type' => 'time_only',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_event_date_1']['group_rows'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_event_date_1']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_event_date_1']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_event_date_1']['field_api_classes'] = 0;
/* Field: Event end time */
$handler->display->display_options['fields']['field_event_date_2']['id'] = 'field_event_date_2';
$handler->display->display_options['fields']['field_event_date_2']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date_2']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date_2']['ui_name'] = 'Event end time';
$handler->display->display_options['fields']['field_event_date_2']['label'] = '';
$handler->display->display_options['fields']['field_event_date_2']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_event_date_2']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_event_date_2']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_event_date_2']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_event_date_2']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_event_date_2']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['settings'] = array(
  'format_type' => 'time_only',
  'fromto' => 'value2',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_event_date_2']['group_rows'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_event_date_2']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_event_date_2']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_event_date_2']['field_api_classes'] = 0;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_event_date] from [field_event_date_1] to [field_event_date_2]';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
$handler->display->display_options['path'] = 'events';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Events';
$handler->display->display_options['menu']['weight'] = '40';
$handler->display->display_options['menu']['name'] = 'main-menu';

/* Display: Attachment - Top 3 */
$handler = $view->new_display('attachment', 'Attachment - Top 3', 'attachment_1');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '3';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['displays'] = array(
  'page' => 'page',
  'default' => 0,
);

/* Display: Manage Events */
$handler = $view->new_display('page', 'Manage Events', 'page_1');
$handler->display->display_options['defaults']['group_by'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_event_date' => 'field_event_date',
  'name' => 'name',
  'changed' => 'changed',
  'status' => 'status',
  'sticky' => 'sticky',
  'view_node' => 'view_node',
  'edit_node' => 'edit_node',
  'delete_node' => 'delete_node',
  'workbench_moderation_history_link' => 'workbench_moderation_history_link',
  'nothing' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = 'field_event_date';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'field_event_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'sticky' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'view_node' => array(
    'align' => '',
    'separator' => '',
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
  ),
  'delete_node' => array(
    'align' => '',
    'separator' => '',
  ),
  'workbench_moderation_history_link' => array(
    'align' => '',
    'separator' => '',
  ),
  'nothing' => array(
    'align' => '',
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Events description */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['ui_name'] = 'Events description';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = '<p>Events are automatically promoted to the front page and appear under the "Events" section of your site.</p>';
$handler->display->display_options['header']['area']['format'] = 'uw_tf_standard';
$handler->display->display_options['header']['area']['tokenize'] = 0;
/* Header: Add event */
$handler->display->display_options['header']['area_1']['id'] = 'area_1';
$handler->display->display_options['header']['area_1']['table'] = 'views';
$handler->display->display_options['header']['area_1']['field'] = 'area';
$handler->display->display_options['header']['area_1']['ui_name'] = 'Add event';
$handler->display->display_options['header']['area_1']['empty'] = TRUE;
$handler->display->display_options['header']['area_1']['content'] = '<ul class="action-links">
<li>
<a href="../../node/add/uw-event">Add event</a>
</li>
</ul>';
$handler->display->display_options['header']['area_1']['format'] = 'uw_tf_standard';
$handler->display->display_options['header']['area_1']['tokenize'] = 0;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Content revision: User */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node_revision';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
$handler->display->display_options['relationships']['uid']['required'] = 0;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Revised by';
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name']['alter']['external'] = 0;
$handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['name']['link_to_user'] = 1;
$handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Last updated';
$handler->display->display_options['fields']['changed']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['changed']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['changed']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['changed']['alter']['external'] = 0;
$handler->display->display_options['fields']['changed']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['changed']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['changed']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['changed']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['changed']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['changed']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['changed']['alter']['trim'] = 0;
$handler->display->display_options['fields']['changed']['alter']['html'] = 0;
$handler->display->display_options['fields']['changed']['element_label_colon'] = 1;
$handler->display->display_options['fields']['changed']['element_default_classes'] = 1;
$handler->display->display_options['fields']['changed']['hide_empty'] = 0;
$handler->display->display_options['fields']['changed']['empty_zero'] = 0;
$handler->display->display_options['fields']['changed']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['changed']['date_format'] = 'time ago';
/* Field: Content: Published */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['status']['alter']['external'] = 0;
$handler->display->display_options['fields']['status']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim'] = 0;
$handler->display->display_options['fields']['status']['alter']['html'] = 0;
$handler->display->display_options['fields']['status']['element_label_colon'] = 1;
$handler->display->display_options['fields']['status']['element_default_classes'] = 1;
$handler->display->display_options['fields']['status']['hide_empty'] = 0;
$handler->display->display_options['fields']['status']['empty_zero'] = 0;
$handler->display->display_options['fields']['status']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Sticky */
$handler->display->display_options['fields']['sticky']['id'] = 'sticky';
$handler->display->display_options['fields']['sticky']['table'] = 'node';
$handler->display->display_options['fields']['sticky']['field'] = 'sticky';
$handler->display->display_options['fields']['sticky']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['external'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['sticky']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['sticky']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['trim'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['html'] = 0;
$handler->display->display_options['fields']['sticky']['element_label_colon'] = 1;
$handler->display->display_options['fields']['sticky']['element_default_classes'] = 1;
$handler->display->display_options['fields']['sticky']['hide_empty'] = 0;
$handler->display->display_options['fields']['sticky']['empty_zero'] = 0;
$handler->display->display_options['fields']['sticky']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['sticky']['not'] = 0;
/* Field: Content: Link */
$handler->display->display_options['fields']['view_node']['id'] = 'view_node';
$handler->display->display_options['fields']['view_node']['table'] = 'node';
$handler->display->display_options['fields']['view_node']['field'] = 'view_node';
$handler->display->display_options['fields']['view_node']['label'] = '';
$handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['view_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['view_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['view_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['view_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['view_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['view_node']['text'] = 'view';
/* Field: Content: Edit link */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['edit_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['edit_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['edit_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['edit_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['edit_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['edit_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['edit_node']['text'] = 'edit';
/* Field: Content: Delete link */
$handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
$handler->display->display_options['fields']['delete_node']['table'] = 'node';
$handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
$handler->display->display_options['fields']['delete_node']['label'] = '';
$handler->display->display_options['fields']['delete_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['delete_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['delete_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['delete_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['delete_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['delete_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['delete_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['delete_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['delete_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['delete_node']['text'] = 'delete';
/* Field: Workbench Moderation: Moderation History Link */
$handler->display->display_options['fields']['workbench_moderation_history_link']['id'] = 'workbench_moderation_history_link';
$handler->display->display_options['fields']['workbench_moderation_history_link']['table'] = 'node';
$handler->display->display_options['fields']['workbench_moderation_history_link']['field'] = 'workbench_moderation_history_link';
$handler->display->display_options['fields']['workbench_moderation_history_link']['label'] = '';
$handler->display->display_options['fields']['workbench_moderation_history_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['external'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['trim'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['html'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['workbench_moderation_history_link']['element_default_classes'] = 1;
$handler->display->display_options['fields']['workbench_moderation_history_link']['hide_empty'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['empty_zero'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['text'] = 'moderate';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = 'Actions';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<ul class="links"><li>[view_node]</li><li>[edit_node]</li><li>[workbench_moderation_history_link]</li><li>[delete_node]</li></ul>';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing']['element_label_colon'] = 1;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['group_type'] = 'count';
$handler->display->display_options['fields']['nid']['label'] = 'GROUPING FIELD TO FORCE DISTINCT';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nid']['alter']['external'] = 0;
$handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nid']['alter']['html'] = 0;
$handler->display->display_options['fields']['nid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nid']['hide_empty'] = 0;
$handler->display->display_options['fields']['nid']['empty_zero'] = 0;
$handler->display->display_options['fields']['nid']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['nid']['format_plural'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_event' => 'uw_event',
);
$handler->display->display_options['filters']['type']['group'] = 0;
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['group'] = 0;
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['required'] = 0;
$handler->display->display_options['filters']['title']['expose']['multiple'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['use_operator'] = FALSE;
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['multiple'] = FALSE;
/* Filter criterion: User: Name */
$handler->display->display_options['filters']['uid']['id'] = 'uid';
$handler->display->display_options['filters']['uid']['table'] = 'users';
$handler->display->display_options['filters']['uid']['field'] = 'uid';
$handler->display->display_options['filters']['uid']['relationship'] = 'uid';
$handler->display->display_options['filters']['uid']['value'] = '';
$handler->display->display_options['filters']['uid']['group'] = 0;
$handler->display->display_options['path'] = 'admin/manage/events';


/* ---------------------------- */

$view = new view();
$view->name = 'uw_news_item_pages';
$view->description = 'News, news-by-date and news-by-category.';
$view->tag = 'news';
$view->base_table = 'node';
$view->human_name = 'News item pages';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'News';
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  4 => '4',
  5 => '5',
  6 => '6',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 0;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_type'] = 'span';
$handler->display->display_options['fields']['title']['element_class'] = 'home-listing-block-link';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Date and time */
$handler->display->display_options['fields']['field_event_date']['id'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date']['field'] = 'field_event_date';
/* Sort criterion: Content: Banner Photo (field_banner_image:fid) */
$handler->display->display_options['sorts']['field_banner_image_fid']['id'] = 'field_banner_image_fid';
$handler->display->display_options['sorts']['field_banner_image_fid']['table'] = 'field_data_field_banner_image';
$handler->display->display_options['sorts']['field_banner_image_fid']['field'] = 'field_banner_image_fid';
/* Sort criterion: Content: Date and time -  start date (field_event_date) */
$handler->display->display_options['sorts']['field_event_date_value']['id'] = 'field_event_date_value';
$handler->display->display_options['sorts']['field_event_date_value']['table'] = 'field_data_field_event_date';
$handler->display->display_options['sorts']['field_event_date_value']['field'] = 'field_event_date_value';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_event' => 'uw_event',
);

/* Display: News - List */
$handler = $view->new_display('page', 'News - List', 'page_1');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '3';
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_news_item' => 'uw_news_item',
);
/* Filter criterion: Content: Post date */
$handler->display->display_options['filters']['created']['id'] = 'created';
$handler->display->display_options['filters']['created']['table'] = 'node';
$handler->display->display_options['filters']['created']['field'] = 'created';
$handler->display->display_options['filters']['created']['operator'] = '>=';
$handler->display->display_options['filters']['created']['value']['value'] = '-1 year';
$handler->display->display_options['filters']['created']['value']['type'] = 'offset';
$handler->display->display_options['path'] = 'news';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'News';
$handler->display->display_options['menu']['weight'] = '45';
$handler->display->display_options['menu']['name'] = 'main-menu';

/* Display: Attachment - Top 3 */
$handler = $view->new_display('attachment', 'Attachment - Top 3', 'attachment_1');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '3';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Sticky */
$handler->display->display_options['fields']['sticky']['id'] = 'sticky';
$handler->display->display_options['fields']['sticky']['table'] = 'node';
$handler->display->display_options['fields']['sticky']['field'] = 'sticky';
$handler->display->display_options['fields']['sticky']['label'] = '';
$handler->display->display_options['fields']['sticky']['exclude'] = TRUE;
$handler->display->display_options['fields']['sticky']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['external'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['sticky']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['sticky']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['trim'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['html'] = 0;
$handler->display->display_options['fields']['sticky']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['sticky']['element_default_classes'] = 1;
$handler->display->display_options['fields']['sticky']['hide_empty'] = 0;
$handler->display->display_options['fields']['sticky']['empty_zero'] = 0;
$handler->display->display_options['fields']['sticky']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['sticky']['type'] = 'sticky';
$handler->display->display_options['fields']['sticky']['not'] = 1;
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = '';
$handler->display->display_options['fields']['created']['alter']['alter_text'] = 1;
$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['created']['alter']['external'] = 0;
$handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
$handler->display->display_options['fields']['created']['alter']['html'] = 0;
$handler->display->display_options['fields']['created']['element_type'] = 'span';
$handler->display->display_options['fields']['created']['element_class'] = 'news-date [sticky]';
$handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['created']['element_default_classes'] = 0;
$handler->display->display_options['fields']['created']['hide_empty'] = 0;
$handler->display->display_options['fields']['created']['empty_zero'] = 0;
$handler->display->display_options['fields']['created']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['created']['date_format'] = 'custom';
$handler->display->display_options['fields']['created']['custom_date_format'] = 'Y-m-d';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_type'] = 'span';
$handler->display->display_options['fields']['title']['element_class'] = 'home-listing-block-link';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['body']['alter']['external'] = 0;
$handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['body']['alter']['trim'] = 0;
$handler->display->display_options['fields']['body']['alter']['html'] = 0;
$handler->display->display_options['fields']['body']['element_type'] = 'div';
$handler->display->display_options['fields']['body']['element_class'] = 'news-summary';
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['element_default_classes'] = 0;
$handler->display->display_options['fields']['body']['hide_empty'] = 0;
$handler->display->display_options['fields']['body']['empty_zero'] = 0;
$handler->display->display_options['fields']['body']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
$handler->display->display_options['fields']['body']['settings'] = array(
  'trim_length' => '600',
);
$handler->display->display_options['fields']['body']['field_api_classes'] = 0;
/* Field: Content: Link */
$handler->display->display_options['fields']['view_node']['id'] = 'view_node';
$handler->display->display_options['fields']['view_node']['table'] = 'node';
$handler->display->display_options['fields']['view_node']['field'] = 'view_node';
$handler->display->display_options['fields']['view_node']['label'] = '';
$handler->display->display_options['fields']['view_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['view_node']['element_type'] = 'div';
$handler->display->display_options['fields']['view_node']['element_class'] = 'read-more-link';
$handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['view_node']['element_default_classes'] = 0;
$handler->display->display_options['fields']['view_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['view_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['view_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['view_node']['text'] = 'Read more';
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_news_item' => 'uw_news_item',
);
/* Filter criterion: Content: Post date */
$handler->display->display_options['filters']['created']['id'] = 'created';
$handler->display->display_options['filters']['created']['table'] = 'node';
$handler->display->display_options['filters']['created']['field'] = 'created';
$handler->display->display_options['filters']['created']['operator'] = '>=';
$handler->display->display_options['filters']['created']['value']['value'] = '-1 year';
$handler->display->display_options['filters']['created']['value']['type'] = 'offset';
$handler->display->display_options['displays'] = array(
  'page_1' => 'page_1',
  'default' => 0,
);

/* Display: News - Archive */
$handler = $view->new_display('page', 'News - Archive', 'page_2');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Created year */
$handler->display->display_options['arguments']['created_year']['id'] = 'created_year';
$handler->display->display_options['arguments']['created_year']['table'] = 'node';
$handler->display->display_options['arguments']['created_year']['field'] = 'created_year';
$handler->display->display_options['arguments']['created_year']['title_enable'] = 1;
$handler->display->display_options['arguments']['created_year']['title'] = '%1';
$handler->display->display_options['arguments']['created_year']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['created_year']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['created_year']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['created_year']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['created_year']['summary_options']['items_per_page'] = '25';
/* Contextual filter: Content: Created month */
$handler->display->display_options['arguments']['created_month']['id'] = 'created_month';
$handler->display->display_options['arguments']['created_month']['table'] = 'node';
$handler->display->display_options['arguments']['created_month']['field'] = 'created_month';
$handler->display->display_options['arguments']['created_month']['title_enable'] = 1;
$handler->display->display_options['arguments']['created_month']['title'] = '%2';
$handler->display->display_options['arguments']['created_month']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['created_month']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['created_month']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['created_month']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['created_month']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_news_item' => 'uw_news_item',
);
$handler->display->display_options['path'] = 'news/archive';

/* Display: News - Category */
$handler = $view->new_display('page', 'News - Category', 'page_3');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Has taxonomy term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['title_enable'] = 1;
$handler->display->display_options['arguments']['term_node_tid_depth']['title'] = '%1';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['term_node_tid_depth']['specify_validation'] = 1;
$handler->display->display_options['arguments']['term_node_tid_depth']['validate']['type'] = 'taxonomy_term';
$handler->display->display_options['arguments']['term_node_tid_depth']['validate_options']['vocabularies'] = array(
  'uwaterloo_audience' => 0,
);
$handler->display->display_options['arguments']['term_node_tid_depth']['validate_options']['type'] = 'tids';
$handler->display->display_options['arguments']['term_node_tid_depth']['validate_options']['transform'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['validate']['fail'] = 'empty';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '2';
$handler->display->display_options['arguments']['term_node_tid_depth']['break_phrase'] = 1;
$handler->display->display_options['arguments']['term_node_tid_depth']['set_breadcrumb'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['use_taxonomy_term_path'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_news_item' => 'uw_news_item',
);
$handler->display->display_options['path'] = 'news/category';

/* Display: News Block - Front Page */
$handler = $view->new_display('block', 'News Block - Front Page', 'block_1');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '3';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 0;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['footer'] = FALSE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['empty'] = FALSE;
$handler->display->display_options['footer']['area']['content'] = '<p class="news-more-link"><a href="news">More news</a></p>';
$handler->display->display_options['footer']['area']['format'] = 'uw_tf_standard';
$handler->display->display_options['footer']['area']['tokenize'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_news_item' => 'uw_news_item',
);
/* Filter criterion: Content: Promoted to front page */
$handler->display->display_options['filters']['promote']['id'] = 'promote';
$handler->display->display_options['filters']['promote']['table'] = 'node';
$handler->display->display_options['filters']['promote']['field'] = 'promote';
$handler->display->display_options['filters']['promote']['value'] = '1';

/* Display: Manage News Items */
$handler = $view->new_display('page', 'Manage News Items', 'manage_news_items');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Manage News Items';
$handler->display->display_options['defaults']['group_by'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '25';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
$handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '10, 25, 50, 100, 200';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'name' => 'name',
  'changed' => 'changed',
  'status' => 'status',
  'sticky' => 'sticky',
  'nid' => 'nid',
  'view_node' => 'view_node',
  'edit_node' => 'edit_node',
  'workbench_moderation_history_link' => 'workbench_moderation_history_link',
  'delete_node' => 'delete_node',
  'nothing' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = 'title';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'sticky' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'nid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'view_node' => array(
    'align' => '',
    'separator' => '',
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
  ),
  'workbench_moderation_history_link' => array(
    'align' => '',
    'separator' => '',
  ),
  'delete_node' => array(
    'align' => '',
    'separator' => '',
  ),
  'nothing' => array(
    'align' => '',
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 1;
$handler->display->display_options['style_options']['empty_table'] = 0;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: News items description */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['ui_name'] = 'News items description';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = '<p>News items are automatically promoted to the front page and appear under the "News" section of your site.</p>';
$handler->display->display_options['header']['area']['format'] = 'uw_tf_standard';
$handler->display->display_options['header']['area']['tokenize'] = 0;
/* Header: Add news item */
$handler->display->display_options['header']['area_1']['id'] = 'area_1';
$handler->display->display_options['header']['area_1']['table'] = 'views';
$handler->display->display_options['header']['area_1']['field'] = 'area';
$handler->display->display_options['header']['area_1']['ui_name'] = 'Add news item';
$handler->display->display_options['header']['area_1']['empty'] = TRUE;
$handler->display->display_options['header']['area_1']['content'] = '<ul class="action-links">
<li>
<a href="../../node/add/uw-news-item">Add news item</a>
</li>
</ul>';
$handler->display->display_options['header']['area_1']['format'] = 'uw_tf_standard';
$handler->display->display_options['header']['area_1']['tokenize'] = 0;
$handler->display->display_options['defaults']['empty'] = FALSE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = '<em>No news items have been found matching your criteria. To add news items, please use the link above.</em>';
$handler->display->display_options['empty']['area']['format'] = 'uw_tf_standard';
$handler->display->display_options['empty']['area']['tokenize'] = 0;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Content revision: User */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node_revision';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
$handler->display->display_options['relationships']['uid']['required'] = 0;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_type'] = 'span';
$handler->display->display_options['fields']['title']['element_class'] = 'home-listing-block-link';
$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
$handler->display->display_options['fields']['title']['element_default_classes'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Revised by';
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name']['alter']['external'] = 0;
$handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['name']['link_to_user'] = 1;
$handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Last updated';
$handler->display->display_options['fields']['changed']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['changed']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['changed']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['changed']['alter']['external'] = 0;
$handler->display->display_options['fields']['changed']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['changed']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['changed']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['changed']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['changed']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['changed']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['changed']['alter']['trim'] = 0;
$handler->display->display_options['fields']['changed']['alter']['html'] = 0;
$handler->display->display_options['fields']['changed']['element_label_colon'] = 1;
$handler->display->display_options['fields']['changed']['element_default_classes'] = 1;
$handler->display->display_options['fields']['changed']['hide_empty'] = 0;
$handler->display->display_options['fields']['changed']['empty_zero'] = 0;
$handler->display->display_options['fields']['changed']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['changed']['date_format'] = 'time ago';
/* Field: Content: Published */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['status']['alter']['external'] = 0;
$handler->display->display_options['fields']['status']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim'] = 0;
$handler->display->display_options['fields']['status']['alter']['html'] = 0;
$handler->display->display_options['fields']['status']['element_label_colon'] = 1;
$handler->display->display_options['fields']['status']['element_default_classes'] = 1;
$handler->display->display_options['fields']['status']['hide_empty'] = 0;
$handler->display->display_options['fields']['status']['empty_zero'] = 0;
$handler->display->display_options['fields']['status']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Sticky */
$handler->display->display_options['fields']['sticky']['id'] = 'sticky';
$handler->display->display_options['fields']['sticky']['table'] = 'node';
$handler->display->display_options['fields']['sticky']['field'] = 'sticky';
$handler->display->display_options['fields']['sticky']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['external'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['sticky']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['sticky']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['trim'] = 0;
$handler->display->display_options['fields']['sticky']['alter']['html'] = 0;
$handler->display->display_options['fields']['sticky']['element_label_colon'] = 1;
$handler->display->display_options['fields']['sticky']['element_default_classes'] = 1;
$handler->display->display_options['fields']['sticky']['hide_empty'] = 0;
$handler->display->display_options['fields']['sticky']['empty_zero'] = 0;
$handler->display->display_options['fields']['sticky']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['sticky']['not'] = 0;
/* Field: Content: Link */
$handler->display->display_options['fields']['view_node']['id'] = 'view_node';
$handler->display->display_options['fields']['view_node']['table'] = 'node';
$handler->display->display_options['fields']['view_node']['field'] = 'view_node';
$handler->display->display_options['fields']['view_node']['label'] = '';
$handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['view_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['view_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['view_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['view_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['view_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['view_node']['text'] = 'View';
/* Field: Content: Edit link */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['edit_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['edit_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['edit_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['edit_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['edit_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['edit_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['edit_node']['text'] = 'Edit';
/* Field: Workbench Moderation: Moderation History Link */
$handler->display->display_options['fields']['workbench_moderation_history_link']['id'] = 'workbench_moderation_history_link';
$handler->display->display_options['fields']['workbench_moderation_history_link']['table'] = 'node';
$handler->display->display_options['fields']['workbench_moderation_history_link']['field'] = 'workbench_moderation_history_link';
$handler->display->display_options['fields']['workbench_moderation_history_link']['label'] = '';
$handler->display->display_options['fields']['workbench_moderation_history_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['external'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['trim'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['alter']['html'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['workbench_moderation_history_link']['element_default_classes'] = 1;
$handler->display->display_options['fields']['workbench_moderation_history_link']['hide_empty'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['empty_zero'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['workbench_moderation_history_link']['text'] = 'Moderate';
/* Field: Content: Delete link */
$handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
$handler->display->display_options['fields']['delete_node']['table'] = 'node';
$handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
$handler->display->display_options['fields']['delete_node']['label'] = '';
$handler->display->display_options['fields']['delete_node']['exclude'] = TRUE;
$handler->display->display_options['fields']['delete_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['delete_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['delete_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['delete_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['delete_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['delete_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['delete_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['delete_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['delete_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['delete_node']['text'] = 'Delete';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = 'Actions';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<ul class="links"><li>[view_node]</li><li>[edit_node]</li><li>[workbench_moderation_history_link]</li><li>[delete_node]</li></ul>';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing']['element_label_colon'] = 1;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['group_type'] = 'count';
$handler->display->display_options['fields']['nid']['label'] = 'GROUPING FIELD TO FORCE DISTINCT';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nid']['alter']['external'] = 0;
$handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nid']['alter']['html'] = 0;
$handler->display->display_options['fields']['nid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nid']['hide_empty'] = 0;
$handler->display->display_options['fields']['nid']['empty_zero'] = 0;
$handler->display->display_options['fields']['nid']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['nid']['format_plural'] = 0;
$handler->display->display_options['defaults']['sorts'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_news_item' => 'uw_news_item',
);
$handler->display->display_options['filters']['type']['group'] = 0;
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['group'] = 0;
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['required'] = 0;
$handler->display->display_options['filters']['title']['expose']['multiple'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['use_operator'] = FALSE;
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['multiple'] = FALSE;
/* Filter criterion: User: Name */
$handler->display->display_options['filters']['uid']['id'] = 'uid';
$handler->display->display_options['filters']['uid']['table'] = 'users';
$handler->display->display_options['filters']['uid']['field'] = 'uid';
$handler->display->display_options['filters']['uid']['relationship'] = 'uid';
$handler->display->display_options['filters']['uid']['value'] = '';
$handler->display->display_options['filters']['uid']['exposed'] = TRUE;
$handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['label'] = 'Revised By';
$handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
$handler->display->display_options['filters']['uid']['expose']['multiple'] = FALSE;
$handler->display->display_options['filters']['uid']['expose']['reduce'] = 0;
$handler->display->display_options['path'] = 'admin/manage/news-items';

/* Display: Events - Category */
$handler = $view->new_display('page', 'Events - Category', 'page_4');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Audience (field_audience) */
$handler->display->display_options['arguments']['field_audience_tid']['id'] = 'field_audience_tid';
$handler->display->display_options['arguments']['field_audience_tid']['table'] = 'field_data_field_audience';
$handler->display->display_options['arguments']['field_audience_tid']['field'] = 'field_audience_tid';
$handler->display->display_options['arguments']['field_audience_tid']['title_enable'] = 1;
$handler->display->display_options['arguments']['field_audience_tid']['title'] = '%1';
$handler->display->display_options['arguments']['field_audience_tid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_audience_tid']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['field_audience_tid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_audience_tid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_audience_tid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_audience_tid']['specify_validation'] = 1;
$handler->display->display_options['arguments']['field_audience_tid']['validate']['type'] = 'taxonomy_term';
$handler->display->display_options['arguments']['field_audience_tid']['validate_options']['vocabularies'] = array(
  'uwaterloo_audience' => 0,
  'uwaterloo_profiles' => 0,
);
$handler->display->display_options['arguments']['field_audience_tid']['validate_options']['type'] = 'tids';
$handler->display->display_options['arguments']['field_audience_tid']['validate_options']['transform'] = 0;
$handler->display->display_options['arguments']['field_audience_tid']['break_phrase'] = 0;
$handler->display->display_options['arguments']['field_audience_tid']['not'] = 0;
/* Contextual filter: Content: Audience (field_audience) */
$handler->display->display_options['arguments']['field_audience_tid_1']['id'] = 'field_audience_tid_1';
$handler->display->display_options['arguments']['field_audience_tid_1']['table'] = 'field_data_field_audience';
$handler->display->display_options['arguments']['field_audience_tid_1']['field'] = 'field_audience_tid';
$handler->display->display_options['arguments']['field_audience_tid_1']['title_enable'] = 1;
$handler->display->display_options['arguments']['field_audience_tid_1']['title'] = '%1';
$handler->display->display_options['arguments']['field_audience_tid_1']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_audience_tid_1']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['field_audience_tid_1']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_audience_tid_1']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_audience_tid_1']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_audience_tid_1']['specify_validation'] = 1;
$handler->display->display_options['arguments']['field_audience_tid_1']['validate']['type'] = 'taxonomy_term';
$handler->display->display_options['arguments']['field_audience_tid_1']['validate_options']['vocabularies'] = array(
  'uwaterloo_audience' => 0,
  'uwaterloo_profiles' => 0,
);
$handler->display->display_options['arguments']['field_audience_tid_1']['validate_options']['type'] = 'tids';
$handler->display->display_options['arguments']['field_audience_tid_1']['validate_options']['transform'] = 0;
$handler->display->display_options['arguments']['field_audience_tid_1']['break_phrase'] = 0;
$handler->display->display_options['arguments']['field_audience_tid_1']['not'] = 0;
$handler->display->display_options['path'] = 'events/category';

/* Display: Events - Archive */
$handler = $view->new_display('page', 'Events - Archive', 'page_5');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Events';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 1;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_type'] = 'span';
$handler->display->display_options['fields']['title']['element_class'] = 'home-listing-block-link';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Date and time */
$handler->display->display_options['fields']['field_event_date']['id'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['table'] = 'field_data_field_event_date';
$handler->display->display_options['fields']['field_event_date']['field'] = 'field_event_date';
$handler->display->display_options['fields']['field_event_date']['label'] = '';
$handler->display->display_options['fields']['field_event_date']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_event_date']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_event_date']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_event_date']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_event_date']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_event_date']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_event_date']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_event_date']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['field_event_date']['settings'] = array(
  'format_type' => 'long',
  'fromto' => 'both',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_event_date']['group_rows'] = 0;
$handler->display->display_options['fields']['field_event_date']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_event_date']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_event_date']['field_api_classes'] = 0;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
$handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_event_date] from [field_event_date_1] to [field_event_date_2]';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['body']['alter']['external'] = 0;
$handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['body']['alter']['trim'] = 0;
$handler->display->display_options['fields']['body']['alter']['html'] = 0;
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['element_default_classes'] = 1;
$handler->display->display_options['fields']['body']['hide_empty'] = 0;
$handler->display->display_options['fields']['body']['empty_zero'] = 0;
$handler->display->display_options['fields']['body']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
$handler->display->display_options['fields']['body']['settings'] = array(
  'trim_length' => '300',
);
$handler->display->display_options['fields']['body']['field_api_classes'] = 0;
/* Field: Content: Link */
$handler->display->display_options['fields']['view_node']['id'] = 'view_node';
$handler->display->display_options['fields']['view_node']['table'] = 'node';
$handler->display->display_options['fields']['view_node']['field'] = 'view_node';
$handler->display->display_options['fields']['view_node']['label'] = '';
$handler->display->display_options['fields']['view_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['view_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['view_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['view_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['view_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['view_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['view_node']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['view_node']['text'] = 'Read more';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Banner Photo (field_banner_image:fid) */
$handler->display->display_options['sorts']['field_banner_image_fid']['id'] = 'field_banner_image_fid';
$handler->display->display_options['sorts']['field_banner_image_fid']['table'] = 'field_data_field_banner_image';
$handler->display->display_options['sorts']['field_banner_image_fid']['field'] = 'field_banner_image_fid';
/* Sort criterion: Content: Date and time -  start date (field_event_date) */
$handler->display->display_options['sorts']['field_event_date_value']['id'] = 'field_event_date_value';
$handler->display->display_options['sorts']['field_event_date_value']['table'] = 'field_data_field_event_date';
$handler->display->display_options['sorts']['field_event_date_value']['field'] = 'field_event_date_value';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Date: Date (node) */
$handler->display->display_options['arguments']['date_argument']['id'] = 'date_argument';
$handler->display->display_options['arguments']['date_argument']['table'] = 'node';
$handler->display->display_options['arguments']['date_argument']['field'] = 'date_argument';
$handler->display->display_options['arguments']['date_argument']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['date_argument']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['date_argument']['granularity'] = 'day';
$handler->display->display_options['arguments']['date_argument']['date_fields'] = array(
  'field_data_field_event_date.field_event_date_value' => 'field_data_field_event_date.field_event_date_value',
);
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'uw_event' => 'uw_event',
);
$handler->display->display_options['path'] = 'events/archive';
