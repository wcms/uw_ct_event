/**
 * @file
 */

(function ($) {
   $(function () {
      $("div.leaflet-container").addClass("element-invisible");
   });
}(jQuery));
