/**
 * @file
 */

(function ($) {
  $(function () {
    // Default state for "Full archive" is closed.
    if (readCookie('Full archive') == null) {
      createCookie('Full archive', 'closed', 30);
    }

    // Removing Calendar button from page if empty.
    if ($('.feed-icon').length) {
      if (!$('.feed-icon').html()) {
        $('.feed-icon').remove();
      }
    }

    // Events by audience and Events by type.
    $('#site-sidebar .block-events h2, #block-views-event-type-block-events-by-type h2').click(function () {
      if ($(this).hasClass('closed')) {
        // Expandable item is closed, open it.
        createCookie($(this).text(), 'open', 30);
        $(this).removeClass('closed');
        $(this).parent('div').find('.item-list').removeClass('hidden');
      }
      else {
        // Expandable item is open, close it.
        createCookie($(this).text(), 'closed', 30);
        $(this).addClass('closed');
        $(this).parent('div').find('.item-list').addClass('hidden');
      }
    }).each(function () {
      if (readCookie($(this).text()) == 'closed') {
        $(this).click();
      }
    });

    // Events by date.
    $('#block-views-events-with-calendar-block-1 h2').click(function () {
      if ($(this).hasClass('closed')) {
        // Expandable item is closed, open it.
        createCookie($(this).text(), 'open', 30);
        $(this).removeClass('closed');
        $parent = $(this).parent('div');
        $parent.find('.view-events-with-calendar, .full-archive').removeClass('hidden');
        // Restore state of full archive if not hidden.
        if (readCookie('Full archive') == 'open') {
          $parent.find('.item-list').removeClass('hidden');
        }
      }
      else {
        // Expandable item is open, close it.
        createCookie($(this).text(), 'closed', 30);
        $(this).addClass('closed');
        $parent = $(this).parent('div');
        $parent.find('.view-events-with-calendar, .full-archive, .item-list').addClass('hidden');
      }
    }).each(function () {
      if (readCookie($(this).text()) == 'closed') {
        $(this).click();
      }
    });

    // Full archive (inside Events by date)
    $('#block-views-events-with-calendar-block-1 h3').click(function () {
      if ($(this).hasClass('closed')) {
        // Expandable item is closed, open it.
        createCookie($(this).text(), 'open', 30);
        $(this).removeClass('closed');
        $(this).parent('div').find('.item-list').removeClass('hidden');
      }
      else {
        // Expandable item is open, close it.
        createCookie($(this).text(), 'closed', 30);
        $(this).addClass('closed');
        $(this).parent('div').find('.item-list').addClass('hidden');
      }
    }).each(function () {
      if (readCookie($(this).text()) == 'closed') {
        $(this).click();
      }
    });

    function createCookie(name,value,days) {
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
      }
      else {
        var expires = "";
      }
      document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
          return c.substring(nameEQ.length,c.length);
        }
      }
      return null;
    }

  });
}(jQuery));
