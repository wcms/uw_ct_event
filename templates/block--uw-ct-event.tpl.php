<?php

/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be "block-user".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 */

$current_theme = variable_get('theme_default', NULL);
$class_current = '';
$class_tab_link = '';
$attrib_block_tab = '';
$id_event = '';
$class_item_class = '';
if ($current_theme == 'uw_fdsu_theme_resp') {
  $class_current = 'block_current';
  $class_tab_link = 'tab-link';
  $attrib_block_tab = 'data-block-tab="event"';
  $id_event = 'event';
  $class_item_class = 'item-class';
}
?>
<div id="<?php print $block_html_id; ?>" class="block-events block-list"<?php print $attributes; ?>>
<?php if ($block->info): ?>
  <?php print render($title_prefix); ?>
  <h2 class="<?php print $class_tab_link; ?>" <?php print $attrib_block_tab; ?><?php print $title_attributes; ?>><?php print $block->subject ?></h2>
  <?php print render($title_suffix); ?>
<?php endif;?>
  <div id="<?php print $id_event; ?>" class="<?php print $class_item_class; ?>"><?php print $content ?></div>
</div>
