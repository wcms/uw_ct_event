<?php

/**
 * @file
 * Template to display a view as a mini calendar month.
 *
 * @see template_preprocess_calendar_mini.
 *
 * $day_names: An array of the day of week names for the table header.
 * $rows: An array of data for each day of the week.
 * $view: The view.
 * $min_date_formatted: The minimum date for this calendar in the format
 *   YYYY-MM-DD HH:MM:SS.
 * $max_date_formatted: The maximum date for this calendar in the format
 *   YYYY-MM-DD HH:MM:SS.
 *
 * $show_title: If the title should be displayed. Normally false since the title
 * is incorporated into the navigation, but sometimes needed, like in the year
 * view of mini calendars.
 */

$params = array(
  'view' => $view,
  'granularity' => 'month',
  'link' => FALSE,
);

// If we aren't in the current month, prepare a "return to current month" link;
// if there isn't a valid date format, redirect to this month.
$query = drupal_get_query_parameters();
$start_date = !empty($query['mini']) ? $query['mini'] : date('Y-m', strtotime($min_date_formatted));
if ($start_date != date('Y-m')) {
  $path = current_path();
  // Calendar starts mishaving somewhere in the 3000s so we don't support the
  // 30th century onward; sorry, future-dwellers.
  if (!empty($query['mini']) && preg_match('/^([12][0-9]{3}-(?:0[1-9]|1[012]|[2-9]|1(?![012]))).+/', $query['mini'], $matches)) {
    // Starts with a valid date, but has to many characters; likely for a
    // specific day, and we only want this to the month level.
    $query['mini'] = date('Y-m', strtotime($matches[1]));
    drupal_set_message(t('Invalid mini-calendar request. You have been redirected to display the closest match.'), 'warning');
    drupal_goto($path, array('query' => $query));
  }
  elseif (!empty($query['mini']) && !preg_match('/^[12][0-9]{3}-(0?[1-9]|1[0-2])$/', $query['mini']) || !$start_date) {
    // Invalid date format, go to current month.
    // Do this instead of unsetting $query['mini'] so that this will still work
    // on big calendar pages not in the current month.
    $query['mini'] = date('Y-m');
    drupal_set_message(t('Invalid mini-calendar request. You have been redirected to display the current month.'), 'warning');
    drupal_goto($path, array('query' => $query));
  }
  else {
    // everything's fine with the date, we just aren't in the current month, so
    // need a link to go back. Do this instead of unsetting $query['mini'] so
    // that this will still work on big calendar pages not in the current month.
    $query['mini'] = date('Y-m');
    $return = l(t('Return to current month'), $path, array('query' => $query));
  }
}
?>
<div class="calendar-calendar"><div class="month-view">
<?php if ($show_title): ?>
<div class="date-nav-wrapper clear-block">
  <div class="date-nav">
    <div class="date-heading">
      <?php print theme('date_nav_title', $params) ?>
    </div>
  </div>
</div>
<?php endif; ?>
<table class="mini">
  <thead>
    <tr>
      <?php foreach ($day_names as $cell): ?>
        <th class="<?php print $cell['class']; ?>">
          <?php print $cell['data']; ?>
        </th>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ((array) $rows as $row): ?>
      <tr>
        <?php foreach ($row as $cell): ?>
          <td id="<?php print $cell['id']; ?>" class="<?php print $cell['class']; ?>">
            <?php print $cell['data']; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div></div><div class="return-current-month">
<?php if (!empty($return)) {
  print $return;
} ?>
</div>
