<?php

/**
 * @file
 * uw_ct_event.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_event_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'event-front_page';
  $context->description = 'Displays event block on a site\'s front page.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_event-front_page' => array(
          'module' => 'uw_ct_event',
          'delta' => 'front_page',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t("Displays event block on a site's front page.");
  $export['event-front_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'event_categories';
  $context->description = 'Displays event categories (taxonomy) blocks.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'events' => 'events',
        'events/*' => 'events/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-events_with_calendar-block_1' => array(
          'module' => 'views',
          'delta' => 'events_with_calendar-block_1',
          'region' => 'sidebar_second',
          'weight' => '-39',
        ),
        'uw_ct_event-events_by_audience' => array(
          'module' => 'uw_ct_event',
          'delta' => 'events_by_audience',
          'region' => 'sidebar_second',
          'weight' => '-37',
        ),
        'views-event_type_block-events_by_type' => array(
          'module' => 'views',
          'delta' => 'event_type_block-events_by_type',
          'region' => 'sidebar_second',
          'weight' => '-36',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays event categories (taxonomy) blocks.');
  $export['event_categories'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'event_feed_ics';
  $context->description = 'Displays event feed and ics block';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'events' => 'events',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_event-event-calendar-block-id' => array(
          'module' => 'uw_ct_event',
          'delta' => 'event-calendar-block-id',
          'region' => 'sidebar_second',
          'weight' => '-41',
        ),
        'uw_ct_event-events_public_feed' => array(
          'module' => 'uw_ct_event',
          'delta' => 'events_public_feed',
          'region' => 'sidebar_second',
          'weight' => '-40',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays event feed and ics block');
  $export['event_feed_ics'] = $context;

  return $export;
}
