<?php

/**
 * @file
 * uw_ct_event.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_event_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_events-tags:admin/structure/taxonomy/uw_event_tags.
  $menu_links['menu-site-manager-vocabularies_events-tags:admin/structure/taxonomy/uw_event_tags'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_event_tags',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Events tags',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_events-tags:admin/structure/taxonomy/uw_event_tags',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_events-type:admin/structure/taxonomy/uw_event_type.
  $menu_links['menu-site-manager-vocabularies_events-type:admin/structure/taxonomy/uw_event_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_event_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Events type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_events-type:admin/structure/taxonomy/uw_event_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Events tags');
  t('Events type');

  return $menu_links;
}
