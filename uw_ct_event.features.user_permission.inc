<?php
/**
 * @file
 * uw_ct_event.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_event_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_event content'.
  $permissions['create uw_event content'] = array(
    'name' => 'create uw_event content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_event content'.
  $permissions['delete any uw_event content'] = array(
    'name' => 'delete any uw_event content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_event content'.
  $permissions['delete own uw_event content'] = array(
    'name' => 'delete own uw_event content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_event content'.
  $permissions['edit any uw_event content'] = array(
    'name' => 'edit any uw_event content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_event content'.
  $permissions['edit own uw_event content'] = array(
    'name' => 'edit own uw_event content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_event revision log entry'.
  $permissions['enter uw_event revision log entry'] = array(
    'name' => 'enter uw_event revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'merge uw_event_tags terms'.
  $permissions['merge uw_event_tags terms'] = array(
    'name' => 'merge uw_event_tags terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'override uw_event authored by option'.
  $permissions['override uw_event authored by option'] = array(
    'name' => 'override uw_event authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_event authored on option'.
  $permissions['override uw_event authored on option'] = array(
    'name' => 'override uw_event authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_event promote to front page option'.
  $permissions['override uw_event promote to front page option'] = array(
    'name' => 'override uw_event promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_event published option'.
  $permissions['override uw_event published option'] = array(
    'name' => 'override uw_event published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_event revision option'.
  $permissions['override uw_event revision option'] = array(
    'name' => 'override uw_event revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_event sticky option'.
  $permissions['override uw_event sticky option'] = array(
    'name' => 'override uw_event sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'submit latitude/longitude'.
  $permissions['submit latitude/longitude'] = array(
    'name' => 'submit latitude/longitude',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'location',
  );

  return $permissions;
}
